#fileToStream

Simple script to redirect a file to a socket with an optional time(seconds) delay between lines.

```bash

bash <( curl -s -L https://bitbucket.org/kellyp/filetostream/downloads/fileToStream ) \
-f path/to/file -h hostname -p port

```